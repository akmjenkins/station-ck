<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-dots="true"
			data-fade="true">
			
			<!-- data-fade="detect" will make this a touch swiper on touch devices, and a fader on non-touch devices -->
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							<a href="#" class="h1-style hero-title">Fitness, Nutrition and Competition Prep.</a>
							
							<p>
								Motivate and educate individuals on how to achieve a healthy balanced lifestyle characterized by physical and mental excellence.
							</p>
							
							<a href="#" class="button outline primary">Read More</a>
						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->

			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							<a href="#" class="h1-style hero-title">Fitness, Nutrition and Competition Prep.</a>
							
							<p>
								Motivate and educate individuals on how to achieve a healthy balanced lifestyle characterized by physical and mental excellence.
							</p>
							
							<a href="#" class="button outline primary">Read More</a>
						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->

			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							<a href="#" class="h1-style hero-title">Fitness, Nutrition and Competition Prep.</a>
							
							<p>
								Motivate and educate individuals on how to achieve a healthy balanced lifestyle characterized by physical and mental excellence.
							</p>
							
							<a href="#" class="button outline primary">Read More</a>
						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="d-bg primary-bg">
		<div class="sw">
		
			<div class="section-excerpt">
			
				<h2 class="section-excerpt-title">Highly quality trainers with proven performance.</h2>
				<p>In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.</p>
			
				<a href="#" class="t-fa-abs fa-plus-circle more">Read More</a>
			
			</div><!-- .section-excerpt -->
		
		</div><!-- .sw -->
	</section><!-- .primary-bg -->
	
	<section class="dark-bg">
		<div class="sw">
		
			<div class="section-excerpt">
			
				<h2 class="section-excerpt-title">The Latest</h2>
				<p>In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.</p>
			
			</div><!-- .section-excerpt -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="nopad">
		<div class="ov-grid grid eqh nopad two-up">
		
			<div class="col">
				<div class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content with-excerpt">
						<a href="#" class="ov-item-title h2-style">You Are Ready for Change! Now What?</a>
						
						<div class="ov-item-excerpt">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
							</p>
							
							<div class="ov-item-excerpt-meta">
							
								<span class="meta d-bg">
									by <a href="#" class="inline">Andy Pratt</a> in <a href="#" class="inline">Blog</a>
								</span><!-- .meta -->
							
								<a href="#" class="t-fa-abs fa-plus-circle more">Read More</a>
							</div><!-- .ov-item-excerpt-meta -->
						</div><!-- .ov-item-excerpt -->
					</div><!-- .ov-item-content -->
					
				</div><!-- .ov-item -->
			</div><!-- .col -->
			
			<div class="col col-2">
				<div class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-2.jpg"></div>
				
					<div class="ov-item-content with-excerpt">
						<a href="#" class="ov-item-title h2-style">It's Never Too Late to Start</a>
						
						<div class="ov-item-excerpt">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
							</p>
							
							<div class="ov-item-excerpt-meta d-bg">
							
								<span class="meta">
									by <a href="#" class="inline">Andy Pratt</a> in <a href="#" class="inline">Blog</a>
								</span><!-- .meta -->
							
								<a href="#" class="t-fa-abs fa-plus-circle more">Read More</a>
							</div><!-- .ov-item-excerpt-meta -->
						</div><!-- .ov-item-excerpt -->
					</div><!-- .ov-item-content -->
				
				</div><!-- .ov-item -->
			</div><!-- .col -->
			
		</div><!-- .ov-grid -->
	</section><!-- .nopad -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="trainer-hero dark-bg">

	<div class="trainer-gallery-wrap">
		<div class="trainer-gallery">
			
			<div class="trainer-gallery-main lazybg" data-src="../assets/images/temp/trainer-1.jpg"></div>
			<div class="trainer-gallery-thumb thumb-one lazybg" data-src="../assets/images/temp/trainer-2.jpg"></div>
			<div class="trainer-gallery-thumb thumb-two lazybg" data-src="../assets/images/temp/trainer-3.jpg"></div>
			
		</div><!-- .trainer-gallery -->
		
		<div class="trainer-contact-bar">
			<a href="#" class="trainer-contact-book">Book Appointment</a>
			<div class="trainer-social">
				<?php include('inc/i-social.php'); ?>
			</div><!-- .trainer-social -->
		</div><!-- .trainer-contact-bar -->
	</div><!-- .trainer-gallery-wrap -->

	<div class="trainer-bio-wrap">
		<div class="trainer-bio">
		
			<div class="article-body">
		
				<div class="hgroup">
					<h1 class="hgroup-title">NJ Hall</h1>
					<span class="hgroup-subtitle">Personal Trainer and Nutritional & Wellness Specialist</span>
				</div><!-- .hgroup -->
				
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing Aenean euismod bibendum gravida dolor sit amet lacus 
					accumsan et viverra justo modo. Proin sodales pulvinar.
				</p>
				
				<p>
					Norma Jean ("NJ") is a wife and mother and a graduate of Memorial University (B.A. Honours Social Psychology). 
					NJ has been interested and involved in fitness and training her whole life but she took that interest to a whole new 
					level following the birth of her son, Zander, by losing 65 lbs., maintaining her new weight and becoming Can Fit Pro 
					Certified as a Personal Trainer Specialist.
				</p>

				<p>
					NJ is also certified in Progressing Nutritional and Wellness and Emergency First Aid, and as an Office Ergonomics Specialist.
				</p>
	 
				<p>
					In 2010, NJ competed in the NLABBA Provincial Championships as a Figure Competitor and she intends to compete in the 
					2011 NLABBA Provincial Championships as well.
				</p>
			
			</div><!-- .article-body -->
		
		</div><!-- .trainer-bio -->
	</div><!-- .trainer-bio-wrap -->
	
</div><!-- .trainer-hero -->

<div class="body">

	<section class="nopad img-section">
		<div class="lazybg" data-src="../assets/images/temp/trainer-bg.jpg">
		</div><!-- .lazybg -->
	</section><!-- .nopad -->
	
	<section>
		<div class="sw">
		
			<div class="grid eqh">
				<div class="col col-3 sm-col-1">
				
					<div class="item dark-bg center service-item">
						<div class="pad-20">
						
							<span class="t-fa-abs fa-rocket">Service One</span>
						
							<h3 class="service-title">Service One</h3>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
								Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
							</p>
						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->
					
				<div class="col col-3 sm-col-1">
					<div class="item dark-bg center service-item">
						<div class="pad-20">
						
							<span class="t-fa-abs fa-rocket">Service Two</span>
						
							<h3 class="service-title">Service Two</h3>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
								Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
							</p>
						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->
					
				<div class="col col-3 sm-col-1">
					<div class="item dark-bg center service-item">
						<div class="pad-20">
						
							<span class="t-fa-abs fa-rocket">Service Three</span>
						
							<h3 class="service-title">Service Three</h3>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
								Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
							</p>
						</div><!-- .pad-20 -->
					</div><!-- .item -->
					
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
		
			<div class="section-excerpt">
			
				<h2 class="section-excerpt-title">Transformations</h2>
				<p>In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.</p>
			
			</div><!-- .section-excerpt -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="nopad">
		<div class="ov-grid grid nopad eqh two-up">
		
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">John Atkins</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">Pat Atkins</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
		</div><!-- .ov-grid -->
	</section><!-- .nopad -->
	
	<section class="dark-bg">
		<div class="sw">
		
			<div class="section-excerpt">
			
				<h2 class="section-excerpt-title">My Blog</h2>
				<p>In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.</p>
			
			</div><!-- .section-excerpt -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="nopad">
			<div class="ov-grid grid nopad eqh three-up">
			
				<div class="col">
					<a href="#" class="ov-item item">
						<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
					
						<div class="ov-item-content">
							<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
							<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
							<time datetime="2014-06-20">Jun 20, 2014</time>
							
							<span class="button fill primary">Read More</span>
						</div><!-- .ov-item-content -->
					
					</a><!-- .ov-item -->
				</div><!-- .col -->

				<div class="col">
					<a href="#" class="ov-item item">
						<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
					
						<div class="ov-item-content">
							<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
							<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
							<time datetime="2014-06-20">Jun 20, 2014</time>
							
							<span class="button fill primary">Read More</span>
						</div><!-- .ov-item-content -->
					
					</a><!-- .ov-item -->
				</div><!-- .col -->

				<div class="col">
					<a href="#" class="ov-item item">
						<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
					
						<div class="ov-item-content">
							<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
							<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
							<time datetime="2014-06-20">Jun 20, 2014</time>
							
							<span class="button fill primary">Read More</span>
						</div><!-- .ov-item-content -->
					
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
			</div><!-- .ov-grid -->
	</section><!-- .nopad -->
	
	<section class="primary-bg d-bg">
		<div class="sw">
		
			<form class="book-appointment body-form full" action="/">
			
				<div class="book-appointment-title">
					<h3 class="hgroup-title">Make an Appointment</h3>
					<p>Schedule an appointment by filling out the form or call me at 709 777 1234.</p>
				</div><!-- .book-appointment-title -->
				
				<div class="book-appointment-fields">
					<input type="text" name="name" placeholder="Full Name">
					<input type="email" name="email" placeholder="Email Address">
					<input type="tel" name="phone" placeholder="Phone Number">
					<button class="button">Book Now</button>
				</div><!-- .book-appointment-fields -->
			
			</form><!-- .book-appointment -->
		
		</div><!-- .sw -->
	</section><!-- .primary-bg -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
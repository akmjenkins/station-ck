<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-dots="true"
			data-fade="true">
			
			<!-- data-fade="detect" will make this a touch swiper on touch devices, and a fader on non-touch devices -->
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							
							<h1 class="hero-title">Trainers</h1>
							
							<p>
								In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.
							</p>

						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="ov-grid grid nopad eqh two-up">
		
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">NJ Hall</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">Susan Dalton</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">NJ Hall</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
			<div class="col">
				<a href="#" class="ov-item item">
					<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
				
					<div class="ov-item-content">
						<span class="ov-item-title">Candace Critch</span>
						<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id purus sit amet elit posuere ultrices. 
							Sed ut sapien luctus, fringilla purus id, pellentesque. Donec id dui a magna sagittis vulputate.
						</p>
						
						<span class="button fill primary">Read More</span>
					</div><!-- .ov-item-content -->
				
				</a><!-- .ov-item -->
			</div><!-- .col -->
			
		</div><!-- .ov-grid -->
	</section><!-- .nopad -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
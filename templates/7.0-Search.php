<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-dots="true"
			data-fade="true">
			
			<!-- data-fade="detect" will make this a touch swiper on touch devices, and a fader on non-touch devices -->
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							
							<h1 class="hero-title">Search Results</h1>
							
							<p>
								We found 10 results for "Query"
							</p>

						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad">

		<div class="filter-section">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-bar-content">
						<div class="filter-bar-left">
							<div class="count">
								<span class="num">10</span> Blog Results
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->
						
						<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
						</div><!-- .filter-bar-meta -->
					</div><!-- .filter-bar-content -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
			
				<div class="ov-grid grid nopad eqh three-up">
				
					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->

					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->

					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->
					
				</div><!-- .ov-grid -->

			</div><!-- .filter-content -->

		</div><!-- .filter-section -->

	</section><!-- .nopad -->
	
	<section class="nopad">

		<div class="filter-section">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-bar-content">
						<div class="filter-bar-left">
							<div class="count">
								<span class="num">10</span> Page Results
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->
						
						<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
						</div><!-- .filter-bar-meta -->
					</div><!-- .filter-bar-content -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
			
				<div class="ov-grid grid nopad eqh three-up">
				
					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->

					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->

					<div class="col">
						<a href="#" class="ov-item item">
							<div class="ov-item-bg lazybg" data-src="../assets/images/temp/block-4.jpg"></div>
						
							<div class="ov-item-content">
								<span class="ov-item-title">Nam at Accumsan Tortor Duis Placerat</span>
								<span class="ov-item-subtitle">Vivamus placerat dolor et nisl pellentesque</span>
								<time datetime="2014-06-20">Jun 20, 2014</time>
								
								<span class="button fill primary">Read More</span>
							</div><!-- .ov-item-content -->
						
						</a><!-- .ov-item -->
					</div><!-- .col -->
					
				</div><!-- .ov-grid -->

			</div><!-- .filter-content -->

		</div><!-- .filter-section -->

	</section><!-- .nopad -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
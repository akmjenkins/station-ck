<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-dots="true"
			data-fade="true">
			
			<!-- data-fade="detect" will make this a touch swiper on touch devices, and a fader on non-touch devices -->
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
				
					<div class="hero-content-wrap">
						<div class="hero-content">					
							
							<h1 class="hero-title">Contact</h1>
							
							<p>
								In hac habitasse platea dictumst. Suspendisse quis interdum quam. Nunc vel magna nisi. Etiam interdum vehicula ultricies. Aliquam erat volutpat.
							</p>

						</div><!-- .hero-content -->
					</div><!-- .hero-content-wrap -->
				
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="map-wrap">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2694.24748480031!2d-52.797774813522345!3d47.52404459711783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0cbcb0ba2e16a7%3A0x7e0ad5d0215b598a!2s874+Topsail+Rd%2C+Mt+Pearl%2C+NL+A1N+3E8!5e0!3m2!1sen!2sca!4v1430430182216" frameborder="0" style="border:0"></iframe>
		</div><!-- .map-wrap -->
	</section><!-- .nopad -->

	<section class="nopad">
		<div class="split-block">
		
			<div class="split-block-item">
				<div class="split-block-content">
					
					<div class="article-body">
					
						<div class="address-business-grid">
						
							<div>

								<h4>Address</h4>
								<address>
									874 Topsail Road <br />
									St. John's, NL <br />
									A1N 3J9
								</address>
								
								<span class="block">Tel: 709 777-1234</span>
								<span class="block">Fax: 709 777-1234</span>
							
							</div>
							
							<div>
							
								<h4>Business Hours</h4>
								<div class="ib">
									<div class="row">
										<span class="l">Monday - Friday</span>
										<span class="r">9:00 to 5PM</span>
									</div><!-- .row -->
									<div class="row">
										<span class="l">Saturday</span>
										<span class="r">10AM to 5PM</span>
									</div><!-- .row -->
									<div class="row">
										<span class="l">Sunday</span>
										<span class="r">Closed</span>
									</div><!-- .row -->
								</div>
								
								<?php include('inc/i-social.php'); ?>
							
							</div>
						</div><!-- .address-business-grid -->
						
						
					</div><!-- .article-body -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
			<div class="split-block-item dark-bg">
				<div class="split-block-content">
					
					<p>Fill out the form below to contact us.</p>
					
					<form action="/" class="body-form">
						<div class="fieldset">
						
							<input type="text" name="name" placeholder="Full Name">
							<input type="email" name="email" placeholder="E-mail Address">
							<input type="tel" name="phone" placeholder="Phone Number">
							<textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>
						
							<button class="button primary outline">Submit</button>
						
						</div><!-- .fieldset -->
					</form><!-- .body-form -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
		</div><!-- .split-block -->
	</section><!-- .nopad -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>